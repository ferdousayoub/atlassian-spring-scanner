package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.springframework.stereotype.Component;

@Component
@ExportAsService
public class PublicNonInterfaceServiceProxy {
    public void a() {
    }
}
