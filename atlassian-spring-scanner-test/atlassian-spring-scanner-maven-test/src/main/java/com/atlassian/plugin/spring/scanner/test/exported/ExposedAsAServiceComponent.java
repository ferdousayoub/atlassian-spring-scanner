package com.atlassian.plugin.spring.scanner.test.exported;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.test.imported.ConsumingMixedComponents;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@ExportAsService
@Component
public class ExposedAsAServiceComponent implements ExposedAsAServiceComponentInterface {
    private final ConsumingMixedComponents consumingMixedComponents;

    @Autowired
    public ExposedAsAServiceComponent(final ConsumingMixedComponents consumingMixedComponents) {
        this.consumingMixedComponents = consumingMixedComponents;
    }

    @Override
    public void doStuff() {
        throw new UnsupportedOperationException("Not implemented");
    }
}
