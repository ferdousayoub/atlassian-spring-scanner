package com.atlassian.plugin.spring.scanner.test.dynamic;

import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.test.InternalComponent;
import com.atlassian.plugin.spring.scanner.test.otherplugin.DynamicallyImportedServiceFromAnotherPlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Due to the '@Profile', this is not instantiated until the "dynamic" profile is loaded by {@link DynamicContextManager}.
 */
@Profile("dynamic")
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@Component
public class DynamicComponent {
    private final DynamicallyImportedServiceFromAnotherPlugin dynamicService;
    private final InternalComponent internalComponent;

    /**
     * @param dynamicService Not referenced elsewhere, so only imported once the dynamic profile is started by {@link DynamicContextManager}
     */
    @Autowired
    public DynamicComponent(@ComponentImport final DynamicallyImportedServiceFromAnotherPlugin dynamicService,
                            final InternalComponent internalComponent) {
        this.dynamicService = dynamicService;
        this.internalComponent = internalComponent;
    }
}
