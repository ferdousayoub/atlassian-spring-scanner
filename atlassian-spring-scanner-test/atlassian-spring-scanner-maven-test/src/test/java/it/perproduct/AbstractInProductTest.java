package it.perproduct;

import com.google.common.io.CharStreams;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public abstract class AbstractInProductTest {

    /**
     * If running from maven, the property is set magically via AMPS, from maven-amps-plugin product config in this module.
     *
     * The AMPS config also overrides this to be the same for every product, so we don't have to have product-specific defaults
     * to run these tests in IDEA.
     */
    protected static final String BASEURL = System.getProperty("baseurl", "http://localhost:5990/product");

    protected static List<String> readStringList(String url) throws IOException {
        final URLConnection connection = new URL(url).openConnection();

        try (InputStream is = connection.getInputStream();
             InputStreamReader reader = new InputStreamReader(is)) {
            return CharStreams.readLines(reader);
        }
    }

    protected static String getUrl(String url) throws IOException {
        final URLConnection connection = new URL(url).openConnection();

        try (InputStream is = connection.getInputStream();
             InputStreamReader reader = new InputStreamReader(is)) {
            return CharStreams.toString(reader);
        }
    }
}
