package com.atlassian.plugin.spring.scanner.core.vfs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

/**
 */
public class CommonIO {
    public static void writeLines(Writer writer, Iterable<String> lines) throws IOException {
        try (BufferedWriter out = new BufferedWriter(writer)) {
            for (String line : lines) {
                out.write(line);
                out.write("\n");
            }
        }
    }

    public static Collection<String> readLines(Reader reader) throws IOException {
        List<String> lines = new ArrayList<String>();
        try (BufferedReader in = new BufferedReader(reader)) {
            String line;
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
        }
        return lines;
    }

    public static void writeProperties(Writer writer, final Properties properties, final String comment)
            throws IOException {
        try (Writer w = writer) {
            properties.store(w, comment);
        }
    }
}
